class Student {
	
	constructor(name, email, grades){
		this.name = name;
		this.email = email;

		this.gradeAve = undefined;
		
		this.passed = undefined;
		this.passedWithHonors = undefined;

		
		if(grades.length === 4){
		    if(grades.every(grade => grade >= 0 && grade <= 100)){
		        this.grades = grades;
		    } else {
		        this.grades = undefined;
		    }
		} else {
		    this.grades = undefined;
		}
	}
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout(){
		console.log(`${this.email} has logged out`);
		return this;
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve(){
	    let sum = 0;
	    this.grades.forEach(grade => sum = sum + grade);
	    // update the gradeAve property
	    this.gradeAve = sum/4;
	    // returns the object
	    return this;
	}
	
	willPass() {
	    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
	    return this;
	}
	
	willPassWithHonors() {
	    if (this.passed) {
	        if (this.gradeAve >= 90) {
	            this.passedWithHonors = true;
	        } else {
	            this.passedWithHonors = false;
	        }
	    } else {
	        this.passedWithHonors = false;
	    }
	    return this;
	}
}


class Section {
	
	constructor(name){
		this.name = name;
		this.students = [];
		this.honorStudents = undefined;
		this.honorsPercentage = undefined;
	}

	
	addStudent(name, email, grades){
		
		this.students.push( new Student(name, email, grades));
	
		return this;
	}

	countHonorStudents(){
		let count = 0;
		this.students.forEach(student => {
			
			if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
				count++;
			}
		})
		this.honorStudents = count;
		return this;
	}
	countHonorsPercentage(){
		this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
		return this;
	}
}


// 1.
class Grade{
    constructor(gradeLevel){
        this.gradeLevel  = gradeLevel;
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }
// 2.
    addSection(name){
        this.sections.push(new Section(name));
        return this;
    }


// 4.
	countStudents(){
        for(let section of this.sections){
            for(let student of section.students){
                this.totalStudents++;
            };            
        };
        return this;
    }

//  5.
    countHonorStudents(){
        for(let section of this.sections){
            for(let student of section.students){
                student.computeAve().willPass().willPassWithHonors();
                student.passedWithHonors?  this.totalHonorStudents++: this.totalHonorStudents
            };            
        };
        return this;
    }

// 6.
    computeBatchAve(){
        countStudents();
        let totalGrades = 0;

        for(let section of this.sections){
            for(let student of section.students){
                totalGrades += student.computeAve().gradeAve;    
            };            
        };
        this.batchAveGrade = totalGrades /  this.totalStudents;
        return this;
    }

// 7.
    getBatchMinGrade(){
        this.batchMinGrade = 100;
        for(let section of this.sections){
            for(let student of section.students){
                for(let grade of student.grades)
                if(grade < this.batchMinGrade) this.batchMinGrade = grade;
            };            
        };
        return this;
    }

// 8.
    getBatchMaxGrade(){
        this.batchMaxGrade = 0;
        for(let section of this.sections){
            for(let student of section.students){
                for(let grade of student.grades)
                if(grade > this.batchMaxGrade) this.batchMaxGrade = grade;
            };            
        };
        return this;
    }

}


//  3.
    const grade1 = new Grade(1);
	grade1.addSection("section1A");

	grade1.sections.find(section => section.name === "section1A").addStudent({email: "john@email.com", name: "john", grades: [89, 84, 78, 88]});

	grade1.sections.find(section => section.name === "section1A").addStudent({email: "joe@email.com", name: "joe", grades: [72, 82, 79, 85]});
	grade1.sections.find(section => section.name === "section1A").addStudent({email: "jane@email.com", name: "jane", grades: [87, 89, 91, 93]});
	grade1.sections.find(section => section.name === "section1A").addStudent({email: "jessie@email.com", name: "jessie", grades: [91, 89, 92, 93]});



